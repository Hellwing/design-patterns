/**
 * DESIGN PATTERN ETAT
 *
 * Ce design pattern s'applique à toute entité dont le cycle de vie est défini par un état et des transitions.
 * L'entité dispose d'un attribut état auquel est délégué son comportement.
 * Ainsi, chaque état est seul à connaître son comportement et aucun algorithme spécifique n'est défini dans l'entité.
 */
package fr.hellwing.designpatterns.etat;