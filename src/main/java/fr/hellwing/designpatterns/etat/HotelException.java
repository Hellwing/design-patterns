package fr.hellwing.designpatterns.etat;

public class HotelException extends Exception {
    public HotelException() {
        super();
    }

    public HotelException(String message) {
        super(message);
    }
}
