package fr.hellwing.designpatterns.etat.Chambre;

import fr.hellwing.designpatterns.etat.HotelException;

import java.text.MessageFormat;

/**
 * Classe représeentant l'état d'une chambre libre
 *
 * Actions possibles (workflow) :
 * - Annuler réservation (réservée->Livre)
 * - Occuper (Réservée->Occupée)
 */
public class ChambreEtatReservee extends ChambreEtat{

    private ChambreEtatReservee() {}
    private static ChambreEtatReservee INSTANCE = new ChambreEtatReservee();

    public static ChambreEtatReservee getInstance() {
        return INSTANCE;
    }
    @Override
    protected void setNombreMaximumOccupants(Chambre chambre, int nombreMaximumOccupants) throws HotelException {
        throw new HotelException(MessageFormat.format("Impossible de modifier le nombre maximum d'occupants de la chambre {0} car elle n'est pas libre.",
                String.valueOf(chambre.numero)));
    }

    @Override
    protected void reserver(Chambre chambre, String nomReservation, int nombreOccupants) throws HotelException {
        throw new HotelException(MessageFormat.format("La chambre {0} est déjà réservée.", String.valueOf(chambre.nombreMaximumOccupants)));
    }
    protected void annulerReservation(Chambre chambre) throws HotelException {
        chambre.nombreOccupants = 0;
        chambre.nomReservation = null;
        chambre.etat = ChambreEtatLibre.getInstance();
    }

    protected void occuper(Chambre chambre) throws HotelException {
        chambre.etat = ChambreEtatOccupee.getInstance();
    }
    protected void quitter(Chambre chambre) throws HotelException {
        throw new HotelException(MessageFormat.format("Impossible de quitter la chambre {0} car elle seulement réservée.", String.valueOf(chambre.numero)));
    }


}
