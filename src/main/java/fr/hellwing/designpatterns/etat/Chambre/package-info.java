/**
 * Ce package représente l'entité Chambre.
 * Il ne contient que la classe Chambre et ses dépendances internes (Id, états...)
 * Dans le Design Pattern Etat, les classes Etat sont les seules à modifier directement les attributs de l'entité
 * (Pas de setters habituels)
 */
package fr.hellwing.designpatterns.etat.Chambre;