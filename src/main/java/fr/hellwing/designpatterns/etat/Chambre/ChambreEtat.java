package fr.hellwing.designpatterns.etat.Chambre;

import fr.hellwing.designpatterns.etat.HotelException;

/**
 * Classe représeentant les états et les actions possibles d'une chambre d'hôtel
 * Etats possibles :
 * - Libre
 * - Réservée
 * - Occupée
 *
 * Actions possibles (workflow) :
 * - Réserver (Livre->Réservée)
 * - Annuler réservation (réservée->Livre)
 * - Occuper (Réservée->Occupée)
 * - Quitter (Occupée->Libre)
 *
 * Les setters pourraient aussi être délégués à l'état, si leur comportement en dépend.
 * Les méthodes au comportement commun à tous les états peuvent être implantées dans cette classes
 * Exemple : Pour des raisons de cohérence, si une partie des setters est identique pour tous les états, mais d'autres non.
 */
public abstract class ChambreEtat {

    //Le nombre maximum d'occupants peut être changé tant que la chambre est libre, mais plus si elle a une réservation
    protected abstract void setNombreMaximumOccupants(Chambre chambre, int nombreMaximumOccupants) throws HotelException;

    //Les méthodes de workflow modifient une partie des attributs
    protected abstract void reserver(Chambre chambre, String nomReservation, int nombreOccupants) throws HotelException;
    protected abstract void annulerReservation(Chambre chambre) throws HotelException;
    protected abstract void occuper(Chambre chambre) throws HotelException;
    protected abstract void quitter(Chambre chambre) throws HotelException;


}
