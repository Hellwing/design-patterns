package fr.hellwing.designpatterns.etat.Chambre;

import fr.hellwing.designpatterns.etat.HotelException;

import java.text.MessageFormat;

/**
 * Classe représeentant l'état d'une chambre libre
 *
 * Actions possibles (workflow) :
 * - Réserver (Livre->Réservée)
 */
public class ChambreEtatLibre extends ChambreEtat{

    private ChambreEtatLibre() {}
    private static ChambreEtatLibre INSTANCE = new ChambreEtatLibre();

    public static ChambreEtatLibre getInstance() {
        return INSTANCE;
    }
    @Override
    protected void setNombreMaximumOccupants(Chambre chambre, int nombreMaximumOccupants) {
        chambre.nombreMaximumOccupants = nombreMaximumOccupants;
    }

    @Override
    protected void reserver(Chambre chambre, String nomReservation, int nombreOccupants) throws HotelException {
        if (nombreOccupants <= chambre.nombreMaximumOccupants) {
            chambre.nomReservation = nomReservation;
            chambre.nombreOccupants = nombreOccupants;
            chambre.etat = ChambreEtatReservee.getInstance();
        } else {
            throw new HotelException(MessageFormat.format("La chambre {0} ne peut accepter que {1} occupants.",
                    String.valueOf(chambre.numero),
                    String.valueOf(chambre.nombreMaximumOccupants)));
        }
    }
    protected void annulerReservation(Chambre chambre) throws HotelException {
        throw new HotelException(MessageFormat.format("Aucune réservation n'a été faite pour la chambre {0}.", String.valueOf(chambre.numero)));
    }
    protected void occuper(Chambre chambre) throws HotelException {
        throw new HotelException(MessageFormat.format("La chambre {0} n'a pas encore été réservée, elle ne peut donc pas être occupée.", String.valueOf(chambre.numero)));
    }
    protected void quitter(Chambre chambre) throws HotelException {
        throw new HotelException(MessageFormat.format("Impossible de quitter la chambre {0} car elle n'est pas encore réservée.", String.valueOf(chambre.numero)));
    }


}
