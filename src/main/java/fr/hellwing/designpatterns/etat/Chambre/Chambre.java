package fr.hellwing.designpatterns.etat.Chambre;

import fr.hellwing.designpatterns.etat.HotelException;

import java.util.Objects;

/**
 * La chambre peut avoir plusieurs état.
 * Toutes les méthodes dépendante de l'état délèguent leur comportement à l'attribut "etat" de lobjet.
 * Ainsi l'objet n'a pas besoin de connaître les différents comportements à adopter en fonction de son état
 *
 * Les actions interdites renvoient une exception
 */
public class Chambre {
    /**
     * Tous les attributs sont protected car ils sont manipulés en accès direct par l'état.
     * L'objet ne doi pas pouvoir mettre à jour ses attributs sans passer par son état.
     * Les états doivent être dans le même package que l'objet lui-même.
     * Aucune autre classe ne doit être dans son package
     */

    //Identifiants, pourraient être regroupés en une classe "Id"
    protected int idHotel;
    protected int numero;

    //Attributs secondaires
    protected ChambreEtat etat;
    protected int nombreMaximumOccupants;
    protected int nombreOccupants;
    protected String nomReservation;

    public Chambre(int idHotel, int numero, int nombreMaximumOccupants) {
        this.idHotel = idHotel;
        this.numero = numero;
        this.nombreMaximumOccupants = nombreMaximumOccupants;
        this.etat = ChambreEtatLibre.getInstance();
    }
    public int getIdHotel() {
        return this.idHotel;
    }

    public int getNumero() {
        return this.numero;
    }

    public void setNombreMaximumOccupants(int nombreMaximumOccupants) throws HotelException {
        this.etat.setNombreMaximumOccupants(this, nombreMaximumOccupants);
    }

    public void reserver(String nomReservation, int nombreOccupants) throws HotelException {
        this.etat.reserver(this, nomReservation, nombreOccupants);
    }
    public void annulerReservation() throws HotelException {
        this.etat.annulerReservation(this);
    };
    public void occuper(Chambre chambre) throws HotelException {
        this.etat.occuper(this);
    };
    public void quitter(Chambre chambre) throws HotelException {
        this.etat.quitter(this);
    };

    /**
     * Equals est utilisé pour identifier un objet sur ses attributs identifiants
     * @param autre Autre objet à comparer
     * @return Vrai si les deux objets sont considérés comme fonctionnellement identiques
     */
    @Override
    public boolean equals(Object autre) {
        if (this == autre) {
            return true;
        }
        else if (!(autre instanceof Chambre)) {
            return false;
        }
        Chambre autreChambre = (Chambre) autre;
        return this.idHotel == autreChambre.getIdHotel()
                && this.numero == autreChambre.getNumero();
    }

    /**
     * Le HashCode doit être généré à partir d'une partie des attributs utilisés par equals
     * Il permet de catégoriser les objets dans l'utilisation de collections Hash*
     * @return HashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.idHotel);
    }



}
