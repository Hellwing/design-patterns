package fr.hellwing.designpatterns.etat.Chambre;

import fr.hellwing.designpatterns.etat.HotelException;

import java.text.MessageFormat;

/**
 * Classe représeentant l'état d'une chambre libre
 *
 * Actions possibles (workflow) :
 * - Annuler réservation (réservée->Livre)
 * - Occuper (Réservée->Occupée)
 */
public class ChambreEtatOccupee extends ChambreEtat{

    private ChambreEtatOccupee() {}
    private static ChambreEtatOccupee INSTANCE = new ChambreEtatOccupee();

    public static ChambreEtatOccupee getInstance() {
        return INSTANCE;
    }
    @Override
    protected void setNombreMaximumOccupants(Chambre chambre, int nombreMaximumOccupants) throws HotelException {
        throw new HotelException(MessageFormat.format("Impossible de modifier le nombre maximum d'occupants de la chambre {0} car elle n'est pas libre.",
                String.valueOf(chambre.numero)));
    }

    @Override
    protected void reserver(Chambre chambre, String nomReservation, int nombreOccupants) throws HotelException {
        throw new HotelException(MessageFormat.format("La chambre {0} est occupée, elle ne peut donc pas encore être réservée.", String.valueOf(chambre.nombreMaximumOccupants)));
    }
    protected void annulerReservation(Chambre chambre) throws HotelException {
        throw new HotelException(MessageFormat.format("La réservation de la chambre {0} ne peut pas être annulée car elle est occupée.", String.valueOf(chambre.nombreMaximumOccupants)));

    }

    protected void occuper(Chambre chambre) throws HotelException {
        throw new HotelException(MessageFormat.format("Impossible d'occuper la chambre {0} car elle est déjà occupée.", String.valueOf(chambre.numero)));
    }
    protected void quitter(Chambre chambre) throws HotelException {
        chambre.nombreOccupants = 0;
        chambre.nomReservation = null;
        chambre.etat = ChambreEtatLibre.getInstance();
    }


}
