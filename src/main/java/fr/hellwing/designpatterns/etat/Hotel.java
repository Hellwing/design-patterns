package fr.hellwing.designpatterns.etat;

import fr.hellwing.designpatterns.etat.Chambre.Chambre;

import java.util.*;

public class Hotel {
    private int id;
    private String nom;
    private List<Chambre> chambres = new ArrayList<>();

    public Hotel(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Ajoute une chambre à l'hôtel
     * Le numéro de chambre est calculé à partir de la chambre au plus grand numéro disponible
     * @param nombreMaximumOccupants Nombre maximum d'occupants que peut accueillir la chambre
     * @return Numéro de la nouvelle chambre
     */
    public int ajouterChambre(int nombreMaximumOccupants) {
        int numeroChambre = getNumeroMaximumChambres() + 1;
        Chambre nouvelleChambre = new Chambre(this.id, numeroChambre, nombreMaximumOccupants);
        this.chambres.add(nouvelleChambre);
        return nouvelleChambre.getNumero();
    }

    public Optional<Chambre> getChambre(int numeroChambre) {
        for (Chambre chambre: chambres) {
            if (chambre.getNumero() == numeroChambre) {
                return Optional.of(chambre);
            }
        }
        return Optional.empty();
    }

    private int getNumeroMaximumChambres() {
        int numeroMax = 0;
        if (!chambres.isEmpty()) {
            //Par construction, la liste est ordonnée par numéro de chambre, et est uniquement manipulée par Hotel
            numeroMax = chambres.get(chambres.size() - 1).getNumero();
        }
        return numeroMax;
    }
}
