/**
 * DESIGN PATTERN BUILDER
 *
 * Ce design pattern permet de séparer la construction d'un objet de sa représentation.
 * Il est utilisé notament lorsqu'un objet est imposant et sa construction complexe (beaucoup d'arguments dans le constructeur)
 * En particulier lorsque l'objet dispose de plusieurs constructeurs différents ou de nombreux paramètres facultatifs
 */
package fr.hellwing.designpatterns.builder;