package fr.hellwing.designpatterns.builder;

/**
 * Menu dont le prix dépend de la présence d'une entrée ou d'un dessert
 *
 * Le menu est constitué d'au moins un plat et une boisson.
 * L'entrée et le dessert sont facultatifs.
 *
 * Pour obtenir un menu il faut faire appel au builder, puis exécuter la méthode build une fois les options appliquées
 */
public class Menu {

    /**
     * Montants pour chaque partie du menu
     */
    public static final double MONTANT_ENTREE = 2.0;
    public static final double MONTANT_PLAT = 8.0;
    public static final double MONTANT_DESSERT = 3.0;

    /**
     * Attributs obligatoires
     */
    private String plat;
    private String boisson;
    /**
     * Attributs facultatifs
     */
    private String entree;
    private String dessert;
    private double montant = 0.0;


    /**
     * Constructeur privé pour obliger le développeur à utiliser le builder
     * Le menu initialise le montant
     * @param builder Builder encapsulant toutes les données de l'objet
     */
    private Menu(MenuBuilder builder) {
        this.plat = builder.plat;
        this.boisson = builder.boisson;
        this.montant = MONTANT_PLAT;
        if (builder.entree != null) {
            this.entree = builder.entree;
            this.montant += Menu.MONTANT_ENTREE;
        }

        if (builder.dessert != null) {
            this.dessert = builder.dessert;
            this.montant += Menu.MONTANT_DESSERT;
        }

    }

    public String getEntree() {
        return entree;
    }

    public String getPlat() {
        return plat;
    }

    public String getBoisson() {
        return boisson;
    }

    public String getDessert() {
        return dessert;
    }

    public double getMontant() {
        return montant;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Menu{");
        sb.append("entree='").append(entree).append('\'');
        sb.append(", plat='").append(plat).append('\'');
        sb.append(", boisson='").append(boisson).append('\'');
        sb.append(", dessert='").append(dessert).append('\'');
        sb.append(", montant=").append(montant);
        sb.append('}');
        return sb.toString();
    }

    /**
     * Builder utilisé pour construire le Menu
     */
    public static class MenuBuilder {

        /**
         * Attributs obligatoires
         */
        private String plat;
        private String boisson;
        /**
         * Attributs facultatifs
         */
        private String entree;
        private String dessert;

        /**
         * NOTE : Le calcul du montant s'effectue dans le constructeur privé de l'objet
         * car ici la responsabilité du builder n'est que de rassembler les données obligatoires
         * et facultatives, en laissant au constructeur la responsabilité des contrôles et calculs
         * complexes.
         *
         * Il est également possible de déléguer cette responsabilité au builder (méthodes with*)
         * car le constructeur de l'objet reste privé
         * En revanche s'il est mutable, ses setters doivent intégrer ces contrôles
         */

        public MenuBuilder(String plat, String boisson) {
            this.plat = plat;
            this.boisson = boisson;
        }

        public MenuBuilder withEntree(String entree) {
            this.entree = entree;
            return this;
        }

        public MenuBuilder withDessert(String dessert) {
            this.dessert = dessert;
            return this;
        }

        public Menu build() {
            return new Menu(this);
        }
    }
}
