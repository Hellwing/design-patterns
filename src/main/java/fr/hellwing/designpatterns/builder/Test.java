package fr.hellwing.designpatterns.builder;

import static org.assertj.core.api.Assertions.*;

public class Test {

    @org.junit.Test
    public void testMenuSimple() {

        String plat = "Entrecôte";
        String boisson = "Bière";

        Menu menuSimple = new Menu.MenuBuilder(plat, boisson).build();
        assertThat(menuSimple.getEntree()).isNull();
        assertThat(menuSimple.getPlat()).isEqualTo(plat);
        assertThat(menuSimple.getBoisson()).isEqualTo(boisson);
        assertThat(menuSimple.getDessert()).isNull();
        assertThat(menuSimple.getMontant()).isEqualTo(Menu.MONTANT_PLAT);
    }


    @org.junit.Test
    public void testMenuEntreePlat() {

        String entree = "Salade";
        String plat = "Entrecôte";
        String boisson = "Bière";

        Menu menuEntreePlat = new Menu.MenuBuilder(plat, boisson)
                .withEntree(entree)
                .build();

        assertThat(menuEntreePlat.getEntree()).isEqualTo(entree);
        assertThat(menuEntreePlat.getPlat()).isEqualTo(plat);
        assertThat(menuEntreePlat.getBoisson()).isEqualTo(boisson);
        assertThat(menuEntreePlat.getDessert()).isNull();
        assertThat(menuEntreePlat.getMontant()).isEqualTo(Menu.MONTANT_PLAT + Menu.MONTANT_ENTREE);
    }


    @org.junit.Test
    public void testMenuPlatDessert() {

        String plat = "Entrecôte";
        String boisson = "Bière";
        String dessert = "Crème brulée";

        Menu menuEntreePlat = new Menu.MenuBuilder(plat, boisson)
                .withDessert(dessert)
                .build();

        assertThat(menuEntreePlat.getEntree()).isNull();
        assertThat(menuEntreePlat.getPlat()).isEqualTo(plat);
        assertThat(menuEntreePlat.getBoisson()).isEqualTo(boisson);
        assertThat(menuEntreePlat.getDessert()).isEqualTo(dessert);
        assertThat(menuEntreePlat.getMontant()).isEqualTo(Menu.MONTANT_PLAT+ Menu.MONTANT_DESSERT);
    }


    @org.junit.Test
    public void testMenuComplet() {

        String entree = "Salade";
        String plat = "Entrecôte";
        String boisson = "Bière";
        String dessert = "Crème brulée";


        Menu menuEntreePlat = new Menu.MenuBuilder(plat, boisson)
                .withEntree(entree)
                .withDessert(dessert)
                .build();

        assertThat(menuEntreePlat.getEntree()).isEqualTo(entree);
        assertThat(menuEntreePlat.getPlat()).isEqualTo(plat);
        assertThat(menuEntreePlat.getBoisson()).isEqualTo(boisson);
        assertThat(menuEntreePlat.getDessert()).isEqualTo(dessert);
        assertThat(menuEntreePlat.getMontant()).isEqualTo(Menu.MONTANT_PLAT + Menu.MONTANT_ENTREE + Menu.MONTANT_DESSERT);
    }
}
