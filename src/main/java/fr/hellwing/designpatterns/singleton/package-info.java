/**
 * DESIGN PATTERN SINGLETON
 *
 * Un singleton est une classe générant une instance unique au sein de l'application.
 * Au lieu d'exposer un constructeur, il met à disposition une méthode permettant de récupérer son unique instance
 */
package fr.hellwing.designpatterns.singleton;