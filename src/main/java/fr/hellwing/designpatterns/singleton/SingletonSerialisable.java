package fr.hellwing.designpatterns.singleton;

import java.io.Serializable;

/**
 * Version minimale d'un singleton sérialisablle.
 * Celui-ci est initialisé dès l'exécution de l'application.
 * La sérialisation nécessite une sécurité supplémentaire pour ne pas créer de nouvelle instance
 */
public class SingletonSerialisable implements Serializable {
    private SingletonSerialisable() {
        //initialisation des éventuelles données du singleton
    }
    private static SingletonSerialisable INSTANCE = new SingletonSerialisable();

    public static SingletonSerialisable getInstance() {
        return INSTANCE;
    }

    //La désérialisation renverra l'instance existante au lieu d'en créer une nouvelle
    private Object readResolve() {
        return INSTANCE;
    }

    /**
     * Méthodes statiques
     */
}
