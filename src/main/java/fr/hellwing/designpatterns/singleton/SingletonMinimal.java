package fr.hellwing.designpatterns.singleton;

/**
 * Version minimale d'un singleton.
 * Celui-ci est initialisé dès l'exécution de l'application.
 */
public class SingletonMinimal {
    private SingletonMinimal() {
        //initialisation des éventuelles données du singleton
    }
    private static SingletonMinimal INSTANCE = new SingletonMinimal();

    public static SingletonMinimal getInstance() {
        return INSTANCE;
    }

    /**
     * Méthodes statiques
     */
}
